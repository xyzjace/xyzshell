#include <stdio.h>
#include <stdlib.h>
#ifndef TRUE
	#define TRUE 1
#endif
#ifndef FALSE
	#define FALSE 0
#endif

/* Warning: Most of these functions perform minimal to no error checking. Read the source before use */

/* returns a pointer to a null terminator at the location of the first occurrence of a delimiter in the string */
char* str_split(char *cpStr,char * cpDelimiters);

/* returns the amount of characters in a given string (null terminator included) */
int str_len(char *cpStr);

/* copies the contents of source into destination */
int str_cpy(char *cpDest, char *cpSource);

/* removes trailing \n and leading whitespace */
char * clean_input(char *cpStr);

/* copies n characters from source to dest */
int strn_cpy(char *cpDest, char *cpSource, int iCharCopyLength);

/* fill string with n amounts of c characters */
void mem_set(char *cpStr,int iBytesToSet, char cSetValue);

/* returns a pointer to index of the command in the command string (eg: returns ls given /usr/bin/ls) */
char * get_command(char *cpBuf);

/* creates an array of strings split on the delimiters provided */
char ** str_to_arr(char *cpStr, char * cpDelimiters,int *iReturnArrSize);

/* returns a pointer to the first occurrence of any character in brk, contained in str. returns the end of the string otherwise */
char * strp_brk(char *cpStr, char * cpBrk);

/* returns a pointer to the first character after all delimiters have been skipped */
char * skip_delimiters(char * cpStr, char * cpDelimiters);

/* returns 1 if a given character is in the string, 0 otherwise */
int str_contains_char(char * cpStr, char cMatch);

/* returns 1 if the substr occurs at the start of str. 0 otherwise */
int str_starts_with(char *cpStr, char *cpSubstr);

/* adds the source string onto the end of the destination string */
char * str_cat(char * cpDest, char * cpSource);

/* returns 1 if s has the same characters as t */
int str_eq(char*cpStrA, char*cpStrB);

/* returns the index at which the string is in the array. -1 otherwise */
int in_array(char *cpStr, char **cpStrArr);

/* returns 1 if the string is all ' ' and '\t', 0 otherwise */
int all_blanks(char * cpStr);

