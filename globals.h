#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/param.h>
#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <dirent.h>
#include "shstrlib.h"

/* maximum allowed characters for shell input from the user */
#ifndef MAX_INPUT_SIZE
#define MAX_INPUT_SIZE 512
#endif

/* not strictly the best definition */
#ifndef TRUE
#define TRUE 1
#endif

/* not strictly the best definition */
#ifndef FALSE
#define FALSE 0
#endif


/* set of enumerators to match against built-in functions */
#define CD_IDX 0
#define PWD_IDX 1
#define ENV_IDX 2
#define SETENV_IDX 3
#define UNSETENV_IDX 4
#define EXIT_IDX 5
#define ECHO_IDX 6

/*
I've written a fair amount of similar string library functions for myself as per the assignment spec.
The functions I've written work comparibly similarly to their original counterparts, and as such
it should be a relatively small task of moving over to standard library functions.

Some of the functions I've written look near identical to their counterparts. It's hard to deviate
from the methods they've used to do the functions.

Others I've written differently in part because of personal taste.
*/

#define strcpy str_cpy
#define strncpy strn_cpy
#define strlen str_len
#define memset mem_set
#define strpbrk strp_brk
#define strcat str_cat

/* global variables */
int iKill = 0;
char *cpShellDelimiters = " \t";
extern char *const *environ;
extern int setenv (const char *, const char *, int);
extern int unsetenv(const char *);

