/*
 * COMP342 Assignment 2
 * Student Number: 41952979
 * Last Modified: 24/April/2012
 *
 * Known bugs: env is likely to have sporadic behaviour
 *
 *
 *
 * Rationale:
 * I have divided the flow of the program into two sections:
 * One part processes in-built commands, and the second part, failing to find an in-built command, attempts to interpret
 * the user input as an executable.
 *
 * A lot of the string functions I wrote are to personal taste, and don't mimic perfectly how standard library functions work.
 * Though this will cause issues if the project is ported to standard libraries, it made sense to write functions which did what I need, as opposed
 * to trying to fit my needs to the tools.
 *
 * I made a few design decision which I feel I should justify:
 *
 * 1: I made some assumptions, which are detailed in this list. I know assumptions can cause issues, thus detailing them at least gives anyone using this shell
 * warning if they check the source.
 *
 * 2: The shell will terminate if a malloc fails. My reasoning behind this is failure to malloc is generally indicative of something sinister, and working
 * in an environment where memory allocation is likely to be rejected over and over is inefficient.
 *
 * 3: The shell excepts the user will delimit arguments with tabs or spaces. Other delimiters could be specified in the source with relative ease.
 * using non whitespace delimiters will cause unusual behavior.
 *
 * 4: I have kept the syntax as similar to BASH as possible. To echo environment variables, they should be prefixed with $ before their name.
 * My reasoning follows that BASH is a widely used and popular shell, thus the syntax will be easily adopted or well-known already. Following from this,
 * future development will also tie closely to BASH syntax.
 *
 *
 * Direction for further work:
 * history
 * child-process killing
 * |
 * ;
 * ~ (almost had this, but I realized I should do it on any argument as opposed to just the first one, so I scrapped the work)
 */
#include "globals.h"

/* Function declarations */

/* reads input from the user into cpBuf and returns the amount of characters read */
int get_input(char *cpBuf);

/* retrieves the current working path with getcwd() and places it in cPath */
void get_current_path(char *cPath);

/* handles SIGINT */
void kill_handler();

/* prints the shell prompt to the user. the prompt is supplied by cpShellPrompt */
void print_prompt(char *cpShellPrompt);

/* processes user input and attempts to run the associated executable */
void process_executable(char *cpCommand, char *cpStart, char *cpEnd, char *cpBuf, int iMaxArgCount, int iBufSize, char **env);

/* print an error if malloc fails */
void mem_allocation_error();

/* returns the absolute path, if possible, of the command given by cpCommand. if it fails, the return value is NULL */
char * find_command_in_path(char * cpCommand);

/* change the current working directory */
void ch_dir(char * cpBuf);

/* print the current working directory supplied by cpPath */
void pwd(char *cpPath);

/* allows execution of commands with a specified environment. also prints the environment variables */
void env(char *cpBuf);

/* sets the environment variable in cpBuf to the value specified in cpBuf */
void set_env(char *cpBuf);

/* unsets the environment variable in cpBuf */
void unset_env(char *cpBuf);

/* exits the shell */
void exit_shell();

/* prints an error to stdout */
void print_error(char * cpErrorMessage);

/* echos a string, or path variable */
void echo(char *cpBuf);

/* main entry point to the shell  */
int main()
{
	/* variable declarations */
	char cInputBuffer[MAX_INPUT_SIZE], *cpBuf = NULL, cPath[MAXPATHLEN], *cpCommand = NULL;
	char *cpBuiltIns[] = {"cd","pwd","env","setenv","unsetenv","exit","echo",NULL};
	int iBufSize=0,iBuiltInIndex=-1;
	cpBuf = cInputBuffer;

	/* zero buffers */
	memset(cInputBuffer,MAX_INPUT_SIZE,'\0');
	memset(cPath,MAXPATHLEN,'\0');

	/* no environment. setup the mandatory variables */
	if(*environ==NULL)
	{
		setenv("PATH","/",1);
		setenv("PWD","/",1);
	}
	else
		get_current_path(cPath); /* retrieve the current working path */

	/* a little candy to satiate the ego */
	setenv("SHELL","xyzShell",1);

	signal(SIGINT,kill_handler); /* register SIGINT handler */

	while(TRUE) /* begin the shell infinite loop */
	{
		int iInBuilt = 0; /* flag to determine if we try to run an executable */


		while(str_len(cpBuf)<1)
		{
			print_prompt(cPath); /* print the shell prompt to the user */

			iBufSize = get_input(cpBuf); /* read user input */
			if((iBufSize==-1 || ((int)cpBuf[0])==0)&& iKill==0) /* invalid user input */
			{
				fputs("\n",stdout);
				return -1;
			}
			if(iKill)
				iKill=0;

		}

		/* cStartP and cEndP are used to define boundaries of tokenized strings as we move through cBufP */
		char *cStartP = cpBuf; /* create a copy of the start of the array as a pointer */
		char *cEndP = cpBuf; /* create a second pointer we'll use to point to the end of strings as we tokenize */
		int iMaxArgCount = (strlen(cpBuf)/2)+1; /* maximum possible arguments in the string (assuming whitespace delimiters) */

		cEndP = str_split(cpBuf,cpShellDelimiters); /* get the first token from the input (the command, usually) */
		cEndP++; /* point to the start of the next string in the input buffer */
		if((iBuiltInIndex = in_array(cpBuf,cpBuiltIns))!=-1) /* built in function check */
		{
			switch(iBuiltInIndex)
			{
				case CD_IDX: /* change directory */
					ch_dir(cEndP);
					get_current_path(cPath);
					break;
				case PWD_IDX: /* print working directory */
					pwd(cPath);
					break;
				case ENV_IDX: /* env */
					env(cEndP);
					break;
				case SETENV_IDX: /* set environment variable */
					set_env(cEndP);
					break;
				case UNSETENV_IDX: /* unset environment variable */
					unset_env(cEndP);
					break;
				case EXIT_IDX: /* exit shell */
					exit_shell(); /* shouldn't need to do any freeing. everything should be freed from the iteration before */
					break;
				case ECHO_IDX: /* echo a string or path variable */
					echo(cEndP);
					break;
				default: /* default case. this should never happen */
					exit_shell();
					break;
			}
			iInBuilt = 1;
		}

		if(iInBuilt==0) /* not an in-built command */
			process_executable(cpCommand,cStartP,cEndP,cpBuf,iMaxArgCount, iBufSize, NULL); /* attempt to execute a command */

		/* reset variables */
		cpCommand = NULL;
		cpBuf = cInputBuffer;
		memset(cpBuf,MAX_INPUT_SIZE,'\0');
		iBuiltInIndex = -1;
		iMaxArgCount = 0;
		iInBuilt = 0;
	}

	return 0;
}

int get_input(char *cpBuf)
{
	char *cRetVal = fgets(cpBuf,MAX_INPUT_SIZE,stdin); /* read from stdin */
	if (cRetVal==NULL) /* no input */
		return -1;
	cpBuf = clean_input(cpBuf); /* removes carriage returns and skips leading whitespace */
	return str_len(cpBuf); /* return the length of the input buffer */
}

void print_prompt(char *cpShellPrompt)
{
	fputs(cpShellPrompt,stdout);
	fputs("> ",stdout);
}
void kill_handler()
{
	iKill = 1;
	fputs("\n",stdout);
	signal(SIGINT,kill_handler); /* re-install the signal handler for SIGINT in case this has removed it */
}
void get_current_path(char *cPath)
{
	char *cPathP = NULL;
	if(NULL==(cPathP = getcwd(cPath,MAXPATHLEN))) /* error retrieving current working directory */
	{
			print_error("Could not retrieve current working directory.");
	}
}
void mem_allocation_error()
{
	print_error("Error allocating memory"); /* call print_error with this message */
	exit(EXIT_FAILURE); /* exit the shell */
}
void process_executable(char *cpCommand, char *cpStart, char *cpEnd, char *cpBuf, int iMaxArgCount, int iBufSize, char **env)
{

	int iPID = 0; /* contains the process ID whilst fork occurs */
	int iExecute = 0; /* flag to give execve the go ahead */
	char **cpArgumentBuffer = NULL; /* holds the arguments supplied to the command */
	int iArgCount=0; /* holds at least the total number of arguments input by the user for the command */


	/* block to check if the command was fully-qualified or relies on the PATH */
	if(cpBuf[0]=='/' || cpBuf[0]=='.') /* likely a fully qualified path */
	{
		/* check access */
		if(access(cpBuf,X_OK)==-1) /* file is not executable, or non-existent */
			print_error("No such file or directory");

		else
		{
			if( ( cpCommand = malloc(( sizeof(char) * strlen(cpBuf) ) )) ==NULL ) /* malloc the size of the command string */
				mem_allocation_error(); /* malloc error */

			iExecute = 1;

			strcpy(cpCommand,cpBuf); /* copy the first token into cCommand */
		}
	}
	else/* check in the PATH for the command */
	{
		if((cpStart = find_command_in_path(cpBuf))==NULL) /* malloc error */
			print_error("No such file or directory");
		else
		{
			if( ( cpCommand = malloc(( sizeof(char) * strlen(cpStart) ) )) ==NULL ) /* malloc the size of the command string */
				mem_allocation_error(); /* malloc error */

			iExecute = 1;

			strcpy(cpCommand,cpStart); /* copy the first token into cCommand */

		}
	}
	if(iExecute) /* everything seems OK, try and collate the arguments and execute the command */
	{

		/* malloc space for the arguments to be stored in */
		if((cpArgumentBuffer = malloc(iMaxArgCount * sizeof(char*)))==NULL)
			mem_allocation_error();


		/* get the name of the command to be passed into argv[0] */
		char *cAppNameP = get_command(cpCommand);
		if((cpArgumentBuffer[iArgCount] = malloc((strlen(cAppNameP))*sizeof(char)))==NULL) /* malloc room for the command name */
		{
			/* free previously declared variables */
			free(cpCommand);
			mem_allocation_error(); /* malloc error */
		}

		strcpy(cpArgumentBuffer[iArgCount],cAppNameP); /* copy command name into the array */

		iArgCount++; /* increment total argument counter */

		int iFirst = 1; /* flag to check if we're dealing with the first argument to the command */
		if(!(all_blanks(cpEnd))) /* treat whitespace as no arguments */
		{
			while( (cpEnd-cpBuf) < iBufSize) /* loop through the first of the string and create the arguments */
			{
				if(iFirst) /* first time in the loop */
				{
					cpStart = cpEnd; /* point to the current argument */
					cpEnd = str_split(cpEnd,cpShellDelimiters); /* tokenize the string and return a pointer to the null terminator */
					iFirst=0; /* flag off */
				}
				else
				{
					cpEnd++; /* increment the pointer to the next argument in the input buffer */
					cpStart = cpEnd; /* take a copy of the location of the string */
					cpEnd = str_split(cpEnd,cpShellDelimiters); /* tokenize the string and return a pointer to the null terminator */
				}

				if((cpArgumentBuffer[iArgCount] = malloc((strlen(cpStart))*sizeof(char)))==NULL) /* malloc space for the tokenized string */
				{
					/* free previously declared variables */
					for(int i = 0; i < iArgCount; i++)
						free(cpArgumentBuffer[iArgCount]);

					free(cpArgumentBuffer);

					free(cpCommand);

					mem_allocation_error(); /* malloc error */

				}

				strcpy(cpArgumentBuffer[iArgCount],cpStart); /* copy the newly tokenized string into the argument buffer */

				iArgCount++; /* increment current argument counter */
			} /* end argument tokenizer while */
		} /* end whitespace argument if */


		cpArgumentBuffer[iArgCount]=(char*)NULL; /* null terminate the list */

		iArgCount++; /* increment to count the null list terminator */



		iPID = fork(); /* fork process */

		if( iPID==0 ) /* child process */
		{
			if(env==NULL) /* use the shell's environment */
			{
				if(execve(cpCommand, cpArgumentBuffer, environ)==-1)
				{
					perror("Error");
				}
			}
			else /* use the environment passed by env() */
			{
				if(execve(cpCommand, cpArgumentBuffer, env)==-1)
				{
					perror("Error");
				}
			}
		}
		else if( iPID < 0 ) /* failed to fork */
		{
			/* free previously declared variables */
			for (int i = 0; i<iArgCount;i++)
				free(cpArgumentBuffer[i]);
			free(cpArgumentBuffer);
			free(cpCommand);

			/* exit the shell */
			exit(EXIT_FAILURE);
		}
		else /* parent */
		{
			/* simply wait until the child process dies and fall back through to the infinite loop */
			int status;
			waitpid(iPID,&status,0);
		}

		/* TODO: potentially keep some or all of this for up-arrow or other purposes */
		/* free variables */
		for (int i = 0; i<iArgCount;i++)
		{
			if(cpArgumentBuffer[i]==NULL) /* boundary check. don't want to be free'ing a NULL */
				break;
			free(cpArgumentBuffer[i]);
		}

		free(cpArgumentBuffer);

	} /* end iExecute if */

	free(cpCommand);

} /* end process_executable() */

char *find_command_in_path(char * cpCommand)
{
	/* for non absolute path commands only */
	char **cPath;
	char *const *cEP = environ;

	int iPathSize = 0;
	for(;*cEP;cEP++)
	{
		if(str_starts_with(*cEP,"PATH")) /* PATH variable in the environment */
		{
			char *cTempPath = malloc(sizeof(char)*strlen(*cEP));
			str_cpy(cTempPath,*cEP); /* create a copy of the path variable to work with */
			cPath = str_to_arr((cTempPath+5),":",&iPathSize); /* array of strings containing PATH directories */
			if(cPath!=NULL)
			{
				for(char ** ctp = cPath; *ctp;ctp++) /* loop through each directory in the path directories array */
				{
					char * cFullCommand = strcat(*ctp,"/"); /* append a / */
					cFullCommand = strcat(cFullCommand,cpCommand); /* append the executable name */
					if(access(cFullCommand,X_OK)!=-1) /* file is executable */
					{
						free(cTempPath);
						return cFullCommand;
					}
				}
			}
			free(cTempPath);
		}
	}

	return NULL;
}

void ch_dir(char *cpBuf)
{
	/* behaves similar to BASH in that supplying no argument or a whitespace argument will return to the users home directory */
	/* if no home directory is found, and whitespace or blank arguments are defined, no directory change is made */
	/* TODO: evaluate ~ to home directory */
	char *const *cEP = environ;
	char * cStartP = cpBuf;
	char *cTempHome = NULL;
	if(strlen(cStartP)==0 || all_blanks(cStartP))
	{
		cStartP = str_split(cpBuf," \t");
		for(;*cEP;cEP++)
		{
			if(str_starts_with(*cEP,"HOME")) /* PATH variable in the environment */
			{
				cTempHome = malloc(sizeof(char)*strlen(*cEP));
				str_cpy(cTempHome,*cEP); /* create a copy of the path variable to work with */
				cStartP = cTempHome+5;
				if(chdir(cStartP)==-1)
					perror("Error: ");
				free(cTempHome);
			}
		}

	}
	else
		if(chdir(cStartP)==-1)
			perror("Error: ");


}
void pwd(char *cpPath)
{
	/* this makes an assumptions that cPath is always updated to properly reflect the current working directory */
	/* it could be retrieved from the environment, but the only place cd occurs is in ch_dir() */
	fputs(cpPath,stdout);
	fputs("\n",stdout);

}
void env(char *cpBuf)
{
	/* I wasn't sure how to use env -a, so I've omitted it for now */
	char **cpEnvVars = NULL;
	char *cCommand = NULL;
	int iEnvSize = 0;
	int iCommandIndex = 0;
	char *const *cEP = environ;
	char *cStartP = NULL;
	char *cEndP = NULL;
	if(str_starts_with(cpBuf,"-i"))
	{
		/* we make an assumption about the data format being:
		 * env -i [pathvar=value...] /path/to/executable [arguments]
		 */
		cpEnvVars = str_to_arr(cpBuf+3,cpShellDelimiters,&iEnvSize); /* what we should get is an array of varname=value's */

		for(;iCommandIndex < iEnvSize; iCommandIndex++) /* loop through the user input and try to find the command the user wants to execute */
			if(!(str_contains_char(cpEnvVars[iCommandIndex],'='))) /* should be the command the user wants to execute */
				break;

		if(iCommandIndex!=iEnvSize) /* valid format */
		{
			if(iCommandIndex==(iEnvSize-1)) /* user has suppled a command with no arguments */
			{
				cStartP = cpEnvVars[iCommandIndex]; /* point to the command */

				for(cEndP=cpEnvVars[iCommandIndex];*cEndP;cEndP++); /* cycle to the end of the environment array and point at the command */

				int iMaxArgCount = 2; /* maximum possible arguments in the string (assuming whitespace delimiters) */

				int iBufSize = strlen(cStartP);

				/* send the command to be executed */
				process_executable(cCommand,cStartP,cEndP,cStartP,iMaxArgCount,iBufSize, cpEnvVars);

				/* free our variables */
				for(int i = 0; i<iEnvSize; i++)
					free(cpEnvVars[i]);
				free(cpEnvVars);


			}
			else if(iCommandIndex<(iEnvSize-1)) /* command with arguments */
			{
				int iRequiredArgBufSize = 0; /* holds the string length of all the arguments the user gave to the command */

				char *cArgBuffer = NULL;

				for(int i = iCommandIndex+1; i<iEnvSize; i++) /* count the string length of all the arguments the user gave to the command */
					iRequiredArgBufSize+=strlen(cpEnvVars[i]);

				if((cArgBuffer=malloc(iRequiredArgBufSize*sizeof(char)))==NULL) /* malloc space for the argument buffer */
					mem_allocation_error();

				memset(cArgBuffer,iRequiredArgBufSize,'\0');

				cStartP = cpEnvVars[iCommandIndex]; /* point to the command name given by the user */

				int iMaxArgCount = 0;

				for(int i = iCommandIndex+1; i<iEnvSize; i++) /* loop through the arguments and create a whitespace delimited string */
				{
					if(iMaxArgCount!=0)
					{
						cArgBuffer = str_cat(cArgBuffer," ");
						cArgBuffer = str_cat(cArgBuffer,cpEnvVars[i]);
					}
					else
						cArgBuffer = str_cat(cArgBuffer,cpEnvVars[i]);
					iMaxArgCount++; /* count how many arguments there are */
				}

				iMaxArgCount++; /* extra one for null terminator in the list */

				cEndP = cArgBuffer; /* point the cEndP to the start of the argument buffer */

				int iBufSize = strlen(cStartP) + strlen(cArgBuffer); /* total "input buffer" size */

				process_executable(cCommand,cStartP,cEndP,cStartP,iMaxArgCount,iBufSize,cpEnvVars);

				/* free previously declared variables */
				for(int i = 0; i<iEnvSize; i++)
					free(cpEnvVars[i]);
				free(cpEnvVars);
				free(cArgBuffer);

			} /* end command-with-arguments if */

			else /* something has gone wrong */
				print_error("No such file or directory");

		} /* end valid format if */

		else /* invalid env -i format */
		{
			/* flag is unrecognized, let's print the syntax */
			fputs("Usage: env -i [pathvar=value,pathvar=value,...] /path/to/executable [arguments]\n",stdout);
			fputs("env -i starts the environment with the exact values specified. Can be started without values.\n",stdout);
			fputs("Usage: env\n",stdout);
			fputs("env will display all current environment variables\n",stdout);
		}

	} /* end -i flag if */

	else /* no flags. print environment to stdout */
	{
		for(;*cEP;cEP++)
		{
			fputs(*cEP,stdout);
			fputs("\n",stdout);
		}
	}
}

void set_env(char *cpBuf)
{
	char *cStartP = cpBuf;
	cpBuf = str_split(cpBuf,"=");
	if(setenv(cStartP,++cpBuf,1)!=0)
		perror("Could not set environment variable: ");
}

void unset_env(char *cpBuf)
{
	char *const *cEP = environ;
	int iFound = 0;
	for(;*cEP;cEP++)
	{
		if(str_starts_with(*cEP,cpBuf))
		{
			if(unsetenv(cpBuf)!=0)
				perror("Could not unset environment variable: ");
			iFound = 1; /* flag that we've found the env variable */
		}
	}
	if(iFound==0) /* no environment variable found. print an error */
	{
		fputs("Environment variable: ",stdout);
		fputs(cpBuf,stdout);
		fputs(" does not exist.\n",stdout);
	}
}
void exit_shell()
{
	exit(EXIT_SUCCESS);
}
void print_error(char * cpErrorMessage)
{
	fputs("Error: ",stdout);
	fputs(cpErrorMessage,stdout);
	fputs("\n",stdout);
}
void echo(char *cpBuf)
{
	int iFound = 0; /* flag to set if environment variable is found */
	char *const *cEP = environ; /* environ iterator */
	if(cpBuf[0]=='$')
	{
		cpBuf++;
		for(;*cEP;cEP++) /* loop through env vars */
		{
			if(str_starts_with(*cEP,cpBuf)) /* if the string is contained in the env */
			{
				fputs(*cEP,stdout);
				iFound = 1;
			}
		}
	}
	if(iFound==0)
		fputs(cpBuf,stdout);
	fputs("\n",stdout);


}

