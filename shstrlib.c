#include "shstrlib.h"
char * skip_delimiters(char * cpStr, char * cpDelimiters)
{
	char * cp= cpStr;
	/* warning: this may return a pointer to the null terminator */
	while(*cp)
	{
		while(str_contains_char(cpDelimiters,(char)*cp))
			cp++;
		if(cp==cpStr) /* if we're at the start of the string, don't skip to the next character */
			return cpStr;
		return ++cp;

	}
	return cp;
}
char* str_split(char *cpStr,char * cpDelimiters)
{
	char *cp = skip_delimiters(cpStr,cpDelimiters);
	if(*cp=='\0')
		return cp;

	cp = strp_brk(cp,cpDelimiters);
	*cp = '\0';
	return cp;
}

int str_len(char *cpStr)
{
	char *cpStrP = cpStr;
	for(;*cpStrP++;);
	return (cpStrP- ++cpStr);
}
int str_cpy(char *cpDest, char *cpSource)
{
  char *cpDestP = cpDest;
  int iCharCount = 0;
  for (;(*cpDestP++ = *cpSource++);iCharCount++);
  return iCharCount;
}
int strn_cpy(char *cpDest, char *cpSource, int iCharCopyLength)
{
	/* copies length characters from source to dest and returns a count of characters copied*/
	int iTotalCharactersCopied = 0;
	char *cpSourceP = cpSource;
	char *cpDestP = cpDest;
	for(;*cpSourceP && iTotalCharactersCopied<iCharCopyLength ;cpDestP++,cpSourceP++,iTotalCharactersCopied++)
		*cpDestP = *cpSourceP;
	return iTotalCharactersCopied;
}
char * clean_input(char *cpStr)
{
	while((*cpStr==' '||*cpStr=='\t')&& *cpStr!='\0')
		cpStr++;
	char *cpStrP = cpStr;
	for(;*cpStrP && *cpStrP!='\n';cpStrP++);
	*cpStrP = '\0';
	return cpStr;
}
void mem_set(char *cpStr,int iBytesToSet, char cSetValue)
{
	for(int i = 0; i<iBytesToSet; i++)
	{
		*cpStr = cSetValue;
		cpStr++;
	}
}
char * get_command(char *cpBuf)
{
	char *cpBufP = cpBuf + str_len(cpBuf)-1; /* end of the character array */
	for(;cpBufP!=cpBuf;cpBufP--)
	{
		if(*cpBufP=='/')
			return ++cpBufP;
	}
	return cpBuf;
}
char ** str_to_arr(char *cpStr, char * cpDelimiters,int *iReturnArrSize)
{
	/* warning: does not null terminate the list */
	int iStrSize = str_len(cpStr);
	int iArgCount = 0;
	char **str_array;
	if( ( str_array = malloc(( sizeof(char*) * iStrSize ) +1 )) == NULL )
	{
		return NULL;
	}
	char *cpStartP = cpStr;
	char *cpEndP = cpStr;


	while( (cpEndP-cpStr) < iStrSize) /* loop through the first of the string and create the arguments */
	{
		if(iArgCount!=0) /* we need it to point to the current address the first time around */
			cpEndP++;

		cpStartP = cpEndP; /* point to the start of the next part of the string */
		cpEndP = str_split(cpEndP,cpDelimiters); /* tokenize the string and return a pointer to the null terminator */
		if((str_array[iArgCount] = malloc((str_len(cpStartP))*sizeof(char)))==NULL)
		{
			for(int i = 0; i < iArgCount; i++)
				free(str_array[iArgCount]);
			free(str_array);
			return NULL;
		}
		str_cpy(str_array[iArgCount],cpStartP); /* copy the newly tokenized string into the argument buffer */
		iArgCount++; /* increment current argument counter */
	}
	*iReturnArrSize = iArgCount;
	return str_array;
}
char * strp_brk(char *cpStr, char * cpBrk)
{
	char *cp;
	char *csp = cpStr;
	while (*csp)
	{
		for (cp = cpBrk; *cp && *cp != *csp; cp++);
		if (*cp)
			return (char*)csp;
		csp++;
	}
	/* i've slightly modified the way strpbrk usually works by returning the end of the string instead of NULL */
	return csp;
}
int str_contains_char(char * cpStr, char cMatch)
{
	char * cp = cpStr;
	for(; *cp; cp++)
		if((char)*cp==cMatch)
			return TRUE;
	return 0;
}
int str_starts_with(char *cpStr, char *cpSubstr)
{
	char * csubp = cpSubstr;
	char * cstrp = cpStr;
	if(str_len(csubp)>str_len(cstrp)) /* substr is longer than string */
		return 0;
	for(; *csubp; csubp++,cstrp++)
		if(((*csubp)!=(*cstrp)) && (*csubp)!='\0')
			return 0;
	return TRUE;

}
char * str_cat(char * cpDest, char * cpSource)
{
	char *cpStartP = cpDest;


	for (; *cpDest; ++cpDest); /* skip to the end of the destination string */

	while ( (*cpDest++ = *cpSource++) != 0); /* append */

	return cpStartP; /* return the string */
}
int str_eq(char*cpStrA, char*cpStrB)
{
	if(str_len(cpStrA)!=str_len(cpStrB))
		return 0;
	char *a = cpStrA;
	char *b = cpStrB;
	for(;(*a) && (*b);a++,b++)
	{
		if((*a)!=(*b))
			return 0;
	}
	return TRUE;
}
int in_array(char *cpStr, char **cpStrArr)
{
	int iRetVal = 0;
	if(str_len(cpStr)<1) /* empty string check */
		return -1;
	for(char **cpIter = cpStrArr; *cpIter!=NULL; cpIter++,iRetVal++)
	{
		if(str_eq(*cpIter,cpStr))
			return iRetVal;
	}
	return -1;
}
int all_blanks(char * cpStr)
{
	char * cpStrP = cpStr;
	for(;*cpStrP;cpStrP++)
		if(*cpStrP!=' ' && *cpStrP!='\t')
			return 0;
	return TRUE;
}


