CC = gcc
CFLAGS	= -Wall -Wextra -std=c99 -pedantic
CDFLAGS = -Wall -Wextra -std=c99 -pedantic -g
MAINMAKE_DEPS = main.o shstrlib.o
MAINMAKE_DEBUG_DEPS = main_d.o shstrlib_d.o
SHSTRLIB_DEPS = shstrlib.c shstrlib.h
MAIN_DEPS = main.c shstrlib.h shstrlib.c
MAIN_DEBUG_DEPS = main
MAKE_NAME = comp342-ass2-41952979
MAKE_DEBUG_NAME = comp342-ass2-41952979-debug
PREFIX=./bin
INSTALL_COMMAND = install -c -m 755

#make for dist
all: main

main: $(MAINMAKE_DEPS)
	$(CC) $(CFLAGS) main.o shstrlib.o -o $(MAKE_NAME)

shstrlib.o: $(SHSTRLIB_DEPS)
	$(CC) $(CFLAGS) -c shstrlib.c -o shstrlib.o

main.o: $(MAIN_DEPS)
	$(CC) $(CFLAGS) -c main.c -o main.o


#debugging
debug: maind

maind: $(MAINMAKE_DEBUG_DEPS)
	$(CC) $(CDFLAGS) main_d.o shstrlib_d.o -o $(MAKE_DEBUG_NAME)

shstrlib_d.o: $(SHSTRLIB_DEPS)
	$(CC) $(CDFLAGS) -c shstrlib.c -o shstrlib_d.o	

main_d.o: $(MAIN_DEBUG_DEPS)
	$(CC) $(CDFLAGS) -c main.c -o main_d.o
	
debug_clean:
	rm *.o
	rm $(MAKE_DEBUG_NAME)


#misc
clean:
	rm *.o

install: all
	mkdir $(PREFIX)
	$(INSTALL_COMMAND) $(MAKE_NAME) $(PREFIX)/$(MAKE_NAME)

distclean: main
	rm $(PREFIX)/$(MAKE_NAME)

